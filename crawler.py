import re, urllib2, urlparse, xml.etree.ElementTree as etree
from parser import Parser
from lib import cleanURL
from urlparse import urlsplit, urlunsplit, urljoin

class Crawler:
    """Crawls through a website
    """

    def __init__(self, url, depth):
        self.root = cleanURL(url)
        self.depth = depth
        self.crawled = {} 
        self.crawling = {}

    def crawl(self):
        """
        Crawls the site. 
        If the crawl was interrupted then it continues from where it stopped.  
        """
        if len(self.crawling) > 0:
            toCrawl = self.crawling
            self.crawling = {}
            for url, depth in toCrawl.iteritems(): self._crawl(url, depth)
        else: 
            self._crawl(self.root, 0)

    def _crawl(self, url, depth):
        if depth > self.depth: return
        if urlsplit(url).netloc.split(".")[-2:] != urlsplit(self.root).netloc.split(".")[-2:]: return
        if url in self.crawling or url in self.crawled: return
        self.crawling[url] = depth
        print "working...", url, depth
        response = None
        html = None
        try:
            response = urllib2.urlopen(url)
            if url != response.geturl():
                print "redirecting to", response.geturl()  
                self._crawl(cleanURL(response.geturl()), depth)
                return
            contentType = response.info().getheader("Content-type").lower()
            if contentType.find("text/html") < 0:
                print "Not a html file" 
                return
            html = response.read()
            if contentType.find("utf-8") >= 0: html = unicode(html, 'utf-8')
        except urllib2.HTTPError as e:
            print 'HTTPError: ', str(e.code)
            return
        except urllib2.URLError as e:
            print 'URLError: ', str(e.reason)
            return
        finally:
            if response: response.close()
        parser = Parser()
        parser.feed(html)
        self.crawled[url] = tuple({cleanURL(urljoin(url, asset)): True for asset in parser.assets})
        del self.crawling[url]
        for url in (cleanURL(urljoin(url, link)) for link in parser.links):
             self._crawl(url, depth + 1)

    def getSiteMap(self):
        """
        Returns the sitemap as an XML tree 
        """
        rootEl = etree.Element('urlset', {
            'xmlns': 'http://www.sitemaps.org/schemas/sitemap/0.9',
            'xmlns:asset': 'http://gocardless.com/schemas/sitemap/asset'})
        for url, assets in self.crawled.iteritems():
            urlEl = etree.SubElement(rootEl, 'url')
            locEl = etree.SubElement(urlEl, 'loc')
            locEl.text = url
            for asset in assets:
                assetEl = etree.SubElement(urlEl, 'asset:asset')
                assetEl.text = asset
        return etree.ElementTree(rootEl)
