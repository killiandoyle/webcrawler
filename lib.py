import re
from urlparse import urlsplit, urlunsplit, SplitResult, parse_qsl
from urllib import urlencode

def cleanURL(url):
    ''' 
    Removes repeating forward slashes in the path and orders the query paramaters 
    '''
    parts = urlsplit(url)
    path = parts.path.strip()
    netloc = parts.netloc.lower()
    path = re.sub(r'\/+', '/', path)
    query = urlencode(sorted(parse_qsl(parts.query)))
    return urlunsplit(SplitResult(parts.scheme, netloc, path, query, ''))

staticPathAttribs = set(("src", "poster"))
staticPathFileRegexAttribs = set(("content", "href"))
staticPathFileRegex = re.compile(r"[A-Za-z0-9/@!$&'(+,;=~\-\.\[\:\]]+\.[a-zA-Z0-9]{1,5}(\?[^\s]*)?[\s]*$")

def isStaticFile(tag, attrName, attrValue):
    """
    Tries to determine if the link is a static file.
    It's a static file if:
     * the tag is not 'a'
     * it's a 'href', 'src' or 'poster' attribute with a value of at least one character
     * it's a 'content' attribute with a format similar to file such that the path can contain 
        any valid url charater but with a suffix of letters or numbers
    """
    attrName = attrName.lower()
    if tag == "a": return False
    if attrName in staticPathAttribs: return len(attrValue.replace("/", "").strip()) > 0
    if not attrName in staticPathFileRegexAttribs: return False
    return staticPathFileRegex.match(attrValue) != None
