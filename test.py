import unittest, urlparse
from crawler import Crawler
from urlparse import urlsplit, SplitResult, urljoin
from parser import Parser
from lib import cleanURL, isStaticFile

class TestParser(unittest.TestCase):
    def test_parser(self):
        parser = Parser()
        parser.feed('''
        <html>
            <header>
                <script src="relscript.js"></script>
                <script src="/absscript.js"></script>
                <link id="id2" rel="stylesheet" href="css.css">
            </header>
            <body>
                <video poster="poster.jpg" src="video.mpeg">
                <div>
                    <img src="img.png">
                </div>
                <a href="rellink.html">
                <a href="/abslink.html">
                <a href="http://localhost/">
            </body>
        </html>''')
        self.assertIn("rellink.html", parser.links)
        self.assertIn("/abslink.html", parser.links)
        self.assertIn("http://localhost/", parser.links)

        self.assertIn("relscript.js", parser.assets)
        self.assertIn("/absscript.js", parser.assets)
        self.assertIn("css.css", parser.assets)
        self.assertIn("/absscript.js", parser.assets)
        self.assertIn("video.mpeg", parser.assets)
    
class TestLib(unittest.TestCase):
    def test_cleanURL(self):
        self.assertEqual(cleanURL("http://host.com/a//b"), "http://host.com/a/b")
        self.assertEqual(cleanURL("host.com?a=1&c=3&b=2"), "host.com?a=1&b=2&c=3")

    def test_isStaticFile(self):
        self.assertTrue(isStaticFile("img", "src", "http://host.com/img.png"))
        self.assertTrue(isStaticFile("link", "href", "css.css"))
        self.assertFalse(isStaticFile("a", "href", "http://host.com/me.png"))
        self.assertTrue(isStaticFile("meta", "content", "https://host.com/path/img.png"))
        self.assertFalse(isStaticFile("meta", "content", "Y80kah87ghJhwiDqw-5ap234p9wCcGt6kMRxvnamtHU"))
        self.assertTrue(isStaticFile("video", "poster", "me.img"))
        self.assertFalse(isStaticFile("video", "poster", ""))

import urllib2
from StringIO import StringIO

class Headers:
    def __init__(self, headers):
        self.headers = headers
        self.iterlist = None
        self.itercount = 0
    
    def getheader(self, name):
        return self.headers[name.lower()]

    def __setitem__(self, name, value):
        self.headers[name] = value
    
    def __iter__(self):
        self.iterlist = self.headers.keys()
        self.itercount = -1
        return self

    def next(self):
        self.itercount = self.itercount + 1
        if self.itercount >= len(self.iterlist) - 1: raise StopIteration
        return self.iterlist[self.itercount]

class CrawlerHTTPHandler(urllib2.HTTPHandler):
    def http_open(self, req):
        body = ""
        code = 200
        msg =  "Ok"
        headers = Headers({"content-type": "text/html; charset=utf-8"}) 
        if req.get_full_url() == "http://test.com":
            code = 301
            msg = "301 Moved Permanently"
            headers["location"] = "http://test.com/"
        elif req.get_full_url() == "http://test.com/":
            body = '''
            <html>
                <header>
                    <script src="rel_script.js"></script>
                    <script src="/abs_script.js"></script>
                    <link id="id2" rel="stylesheet" href="/css/css.css">
                </header>
                <body>
                    <video poster="poster.jpg" src="video.mpeg">
                    <img src="rel_path/img.jpeg">
                    <div>
                        <img src="/abs_path/img.png">
                    </div>
                    <a href="link1">
                    <a href="/link2/">
                    <a href="http://bad.com/bad.html">
            </body></html>'''
        elif req.get_full_url() == "http://test.com/link1":
            body = '''
            <html>
                <body>
                    <img src="rel_path/img.png">
                    <img src="/abs_path/img.png">
                    <a href="">
                    <a href="link1">
                    <a href="http://test.com/link1">
                    <a href="#me">
                    <a href="/link11">
                    <a href="http://bad.com/">
                    <a href="/link2/">                    
                </body>
            </html>'''
        elif req.get_full_url() == "http://test.com/link11":
            body = '''<html><body>
                <a href="http://bad.com/bad.html">
            </body></html>'''
        elif req.get_full_url() == "http://test.com/link2/":
            body = '''<html>
                <header>
                    <script src="rel_script.js"></script>
                    <script src="/abs_script.js"></script>
                </header>
                <body></body></html>'''
        else:
            code = 404
            msg = "Not Found"
        resp = urllib2.addinfourl(StringIO(body), headers, req.get_full_url())
        resp.code = code
        resp.msg = msg
        return resp

class TestCrawler(unittest.TestCase):
    def setUp(self):
        urllib2.install_opener(urllib2.build_opener(CrawlerHTTPHandler))

    def test_depth_0(self):
        crawler = Crawler("http://test.com/", 0)
        crawler.crawl()
        self.assertEquals(len(crawler.crawled), 1)

    def test_depth_1(self):
        crawler = Crawler("http://test.com/", 1)
        crawler.crawl()
        self.assertEquals(len(crawler.crawled), 3)
        self.assertIn("http://test.com/", crawler.crawled)
        self.assertIn("http://test.com/link1", crawler.crawled)
        self.assertIn("http://test.com/link2/", crawler.crawled)
        self.assertNotIn("http://test.com/link2", crawler.crawled)

    def test_depth_999(self):
        crawler = Crawler("http://test.com/", 999)
        crawler.crawl()
        self.assertIn("http://test.com/", crawler.crawled)
        self.assertIn("http://test.com/link1", crawler.crawled)
        self.assertIn("http://test.com/link11", crawler.crawled)
        self.assertIn("http://test.com/link2/", crawler.crawled)
        self.assertNotIn("http://test.com/link2", crawler.crawled)
        self.assertNotIn("http://bad.com/bad.html", crawler.crawled)
        self.assertNotIn("http://bad.com/", crawler.crawled)

    def test_asset(self):
        crawler = Crawler("http://test.com/", 999)
        crawler.crawl()

        assets = crawler.crawled["http://test.com/"]
        self.assertEqual(len(assets), 7) 
        self.assertIn("http://test.com/abs_script.js", assets)
        self.assertIn("http://test.com/rel_script.js", assets)
        self.assertIn("http://test.com/css/css.css", assets)
        self.assertIn("http://test.com/rel_path/img.jpeg", assets)
        self.assertIn("http://test.com/abs_path/img.png", assets)
        self.assertIn("http://test.com/poster.jpg", assets)
        self.assertIn("http://test.com/video.mpeg", assets)
        
        assets = crawler.crawled["http://test.com/link1"]
        self.assertEqual(len(assets), 2)
        self.assertIn("http://test.com/abs_path/img.png", assets)
        self.assertIn("http://test.com/rel_path/img.png", assets)

        assets = crawler.crawled["http://test.com/link2/"]
        self.assertEqual(len(assets), 2)
        self.assertIn("http://test.com/link2/rel_script.js", assets)
        self.assertIn("http://test.com/abs_script.js", assets)

if __name__ == '__main__':
    unittest.main()
