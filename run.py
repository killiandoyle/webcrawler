import sys
from urlparse import urlsplit
from crawler import Crawler

if __name__ == "__main__":    
    url = "http://gocardless.com"
    outputFilename = "sitemap.xml"
    depth = 1
    try: 
        if len(sys.argv) > 1: 
            url = urlsplit(sys.argv[1])
        if len(sys.argv) > 2:
            outputFilename = sys.argv[2]
        if len(sys.argv) > 3: 
            depth = int(sys.argv[3])
    except Exception as e:
        print '''
            Error - %s
            usage: app.py domain outputfilename [maxdepth]
            default: app.py http://gocardless.com sitemap.xml 99''' % e.message
        sys.exit()
    
    crawler = Crawler(url, depth)
    crawler.crawl()
    crawler.getSiteMap().write(outputFilename)
