import re, xml.etree.ElementTree as etree
from HTMLParser import HTMLParser
from lib import isStaticFile

class Parser(HTMLParser):
    """Parses a webpage for links and static files
    """

    def __init__(self):
        HTMLParser.__init__(self)
        self.links = []
        self.assets = []

    def handle_starttag(self, tag, attrs):
        attrs = dict(attrs)
        if tag == "a" and "href" in attrs:
            self.links.append(attrs["href"])
        else:
            for name, value in attrs.iteritems():
                if value == None: continue
                value = value.strip()
                if not isStaticFile(tag, name, value): continue
                self.assets.append(value)
